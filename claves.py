import random


def maximo_comun_divisor(a, b):
    """
    Realiza el algoritmo de euclides para tener el MCD de a y b
    """
    if b == 0:
        return a
    else:
        return maximo_comun_divisor(b, a % b)


def euclides_ext(a, b):
    """
    Realiza el algoritmo de Euclides extendido
    retorna el MCD, coeficiente de a y de b
    """
    x, x_0 = 0, 1
    y, y_0 = 1, 0

    while b != 0:
        cociente = a // b
        a, b = b, a - cociente * b
        x_0, x = x, x_0 - cociente * x
        y_0, y = y, y_0 - cociente * y

    return a, x_0, y_0


def elegir_k(z):
    """
    Elige un numero aleatorio e, tal que 1 < e < z, y verifica si es coprimo de z
    """

    k = random.randrange(2, z)
    return k if maximo_comun_divisor(k, z) == 1 else elegir_k(z)


def generar(identificador: str):
    """
    Genera dos claves una publica y una privada usando el algoritmo de RSA
    """

    rand1 = random.randint(9000, 9500)
    rand2 = random.randint(9000, 9500)

    # Revisa el archivo de numero primos
    fo = open('primos.in', 'r')
    lines = fo.read().splitlines()
    fo.close()

    primo1 = int(lines[rand1])
    primo2 = int(lines[rand2])

    # calculo de n, z y k
    n = primo1 * primo2
    z = (primo1 - 1) * (primo2 - 1)
    k = elegir_k(z)

    # calcula j haciendo el algoritmo de euclides extendido
    _, x, y = euclides_ext(k, z)

    if x < 0:
        j = x + z
    else:
        j = x

    # llenando los archivos con sus respectivas claves
    f_pub = open('clave_publica_{}.txt'.format(identificador), 'w')
    f_pub.write(str(n) + '\n')
    f_pub.write(str(k) + '\n')
    f_pub.close()

    f_priv = open('clave_privada_{}.txt'.format(identificador), 'w')
    f_priv.write(str(n) + '\n')
    f_priv.write(str(j) + '\n')
    f_priv.close()

    print("Claves generadas exitosamente, clave_publica_{}.txt y clave_privada_{}.txt".format(identificador, identificador))
