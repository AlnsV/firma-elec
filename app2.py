import Crypto 

from Crypto.PublicKey import RSA

import binascii

from Crypto.Hash import SHA256
from Crypto.Signature import PKCS1_v1_5

"""Generador de claves publica y privada"""
random_generator = Crypto.Random.new().read

private_key = RSA.generate(1024, random_generator)
public_key = private_key.publickey()

private_key = private_key.exportKey(format='DER')
public_key = public_key.exportKey(format='DER')

private_key = binascii.hexlify(private_key).decode('utf8')
public_key = binascii.hexlify(public_key).decode('utf8')

"""Firma electronica"""
signer = PKCS1_v1_5.new(private_key)

resource = 'Texto plano!'
resource = resource.encode()

sha = SHA256.new()
sha.update(resource)

signature = signer.sign(sha)

"""Verificando firma"""

signer = PKCS1_v1_5.new(public_key)

sha = SHA256.new()
sha.update(resource) #Recurso

"""result = signer.verify(sha, signature)""" 

if signer.verify(sha, signature):
    print('¡Bien, la firma es válida!')
else:
    print('No, el mensaje fue firmado con la clave privada incorrecta o modificado')