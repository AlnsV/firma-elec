import fire

from claves import generar

"""
Para conocer modo de uso, correr:

python3 app.py
"""


class Encrypthor(object):
    """Clase para cifrar y decifrar mensaje usando RSA y claves publicas/privadas"""

    @staticmethod
    def generar_claves(id_clave: str):
        """Genera la clave publica y la privada con el siguiente formato:
        publica: clave_publica.txt
        privada: clave_privada.txt
        """
        generar(id_clave)

    @staticmethod
    def cifrar_mensaje(nombre_archivo_clave: str):
        pass
        # aqui va la app 2

    @staticmethod
    def descifrar_mensaje(nombre_archivo_clave: str):
        pass
        # aqui va la app 3


if __name__ == '__main__':
    fire.Fire(Encrypthor)
